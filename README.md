![](https://i.vgy.me/IcwEjs.png)
A chat program I'm making with QB64. 

------

### Disclaimers
* If something breaks, it's not our fault. We'll try our best to help, but it isn't our job to make everything perfect for you.
* All data transmitted and logged on your servers or systems is your fault, so if anything illegal happens don't come to us about it.
* Any issues with QB64 itself should be dealt with the proper team/developers - *not us*.

### Get Started
You can get help with the program, and information on how to set it up on our wiki page [here](https://gitlab.com/noahmarshall12/minichat/-/wikis/home).

### Official Chat Server
You can use [this](https://pastr.io/raw/KoGEtpSvb2A) configuration if you want to connect to our official server.

### Discord
If you need help, join our Discord server [here](https://discord.gg/Raa5Wz).
